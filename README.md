# Enos Consul

A tool to automate the deployment of Consul on a Kubernetes cluster using [enoslib](https://github.com/BeyondTheClouds/enoslib)


## Installation

On a Grid'5000 frontend:

```bash
git clone https://gitlab.inria.fr/aszymane/enos-consul.git
```

Create and/or activate a virtual environment
```bash
virtualenv -p python3 venv 
source venv/bin/activate
pip install -U pip
pip install enoslib
```

```bash
cd enos-consul
```

If you don't already have a ~/.python-grid5000.yaml file with verify_ssl: False (or anything better)
```bash
cat > ~/.python-grid5000.yaml <<EOF 
verify_ssl: False
EOF

chmod 600 ~/.python-grid5000.yaml
```

## Launch

Launch with `python enos-consul.py`. It will take a while to complete (~ 10min)

### Acces Consul UI on your browser

First check all pods are running. On your master node, run :
```bash
kubectl get pods 
```
If all pods are both in STATUS **Running** AND READY **1/1** (otherwise just wait), then find the IP for consul-ui :

```bash
kubectl get services
```
Out of all the pods, spot the one named *hashicorp-consul-ui*. Something like this :

```
hashicorp-consul-ui   ClusterIP   10.101.117.247   <none>   80/TCP
```

Note the IP. Now on your local machine, run the command (replace IP with the one you have and MASTER with your master node, like parasilo-1 for ex) :

```bash
ssh -L 8080:IP:80 MASTER.rennes.g5k -l root
```

Access at http://localhost:8080





